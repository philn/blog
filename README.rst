
N.B: Use this under the pelican virtualfish:
::

    $ # Installation, see https://github.com/justinmayer/virtualfish
    $ vf new pelican

    $ vf activate pelican
    $ pip install pelican[markdown] typogrify
    $ ...
    $ vf deactivate

In ~/.config/starship.toml:
::

   [env_var.VIRTUAL_ENV_NAME]
   format = "in venv [$env_value]($style) "
   style = "purple"
