Coherence and Nautilus, UPnP a bit more on the desktop
######################################################
:date: 2008-10-27 15:49:39
:modified: 2008-10-27 15:55:31
:tags: Linux
:slug: 104

Frank_ is at it again on his quest to make Coherence conquer the GNOME desktop. This time Nautilus is part of the game, allowing the user to easily share files over UPnP and easily select a MediaRenderer to play media files.

Checkout the video on his post_, it is just awesome! Thanks Frank for all this work, hoping it will give ideas to others :)

.. _Frank: http://netzflocken.de
.. _post: http://netzflocken.de/2008/10/26/coherence-and-nautilus-brothers-in-arms