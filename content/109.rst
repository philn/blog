Coherence team at GCDS
######################
:date: 2009-07-09 12:58:30
:modified: 2009-07-12 20:27:47
:tags: Python, Projects
:slug: 109

I gave a talk_ about Coherence_ and Telepathy_ at Gran Canaria
Desktop Summit with Frank_ on tuesday. It went well, there were
about 50 people. I think most of the people got the concept we
tried to explain: UPnP devices and services are not bound to
local network anymore, thanks to the combination of Coherence and
XMPP through Telepathy. The slides should be online soon, will post
an update to this post when I know the url ;)

Part of the Coherence gang including Benjamin, Frank and me is
here at Las Palmas, feel free to grab us if you have any
question. It was also nice to meet Karl Vollmer, the Ampache_
lead developer :)

So we are quite excited about Mirabeau, work on it will continue
during the summer and a first release should happen in
september. I'm writing a little howto explaining how to easily
try the current (work-in-progress) version of Mirabeau, it will
land in the wiki in the coming days ;)

**update**: Slides are there_

.. _talk: http://www.grancanariadesktopsummit.org/node/203
.. _Coherence: http://coherence-project.org
.. _Telepathy: http://telepathy.freedesktop.org
.. _Frank: http://netzflocken.de
.. _Ampache: http://ampache.org
.. _there: http://coherence-project.org/download/mirabeau-guadec-2009.odp