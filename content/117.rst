WebKitGTK+ and HTML5 fullscreen video
#####################################
:date: 2010-08-20 12:47:26
:modified: 2017-01-11 15:42:00
:slug: 117

`HTML5 video`_ is really nice and all but one annoying thing is the
lack of fullscreen video specification, it is currently up to the
User-Agent to allow fullscreen video display.

WebKit allows a fullscreen button in the media controls and
Safari can, on user-demand only, switch a video to fullscreen and
display nice controls over the video. There's also a
webkit-specific DOM API that allow custom media controls to also
make use of that feature, as long as it is initiated by a user
gesture. Vimeo's HTML5 player uses that feature for instance.

Thanks to the efforts made by Igalia_ to improve the WebKit GTK+ port and its GStreamer media-player, I have been able to implement support for this fullscreen video display feature. So `any application`_ using WebKitGTK+ can now make use of it :)

The way it is done with this first implementation is that a new
fullscreen gtk+ window is created and our GStreamer media-player
inserts an autovideosink in the pipeline and overlays the video
in the window. Some simple controls are supported, the UI is
actually similar to Totem's. One improvement we could do in the
future would be to allow application to override or customize
that simple controls UI.

The nice thing about this is that we of course use the
`GstXOverlay`_ interface and that it's implemented by the linux,
windows and mac os video sinks :) So when other WebKit ports
start to use our GStreamer player implementation it will be
fairly easy to port the feature and allow more platforms to
support fullscreen video display :)

Benjamin_ also suggested that we could reuse the cairo surface of
our WebKit videosink and paint it in a fullscreen GdkWindow. But
this should be done only when his Cairo/Pixman patches to enable
hardware acceleration land.

So there is room for improvement but I believe this is a first
nice step for fullscreen HTML5 video consumption from Epiphany_
and other WebKitGTK+-based browsers :) Thanks a lot to Gustavo_,
Sebastian DrÃ¶ge and Martin_ for the code-reviews.

.. _Igalia: http://igalia.com
.. _HTML5 video: https://en.wikipedia.org/wiki/HTML5_video
.. _any application: http://trac.webkit.org/wiki/ApplicationsGtk
.. _GstXOverlay: http://www.gstreamer.net/data/doc/gstreamer/head/gst-plugins-base-libs/html/gst-plugins-base-libs-gstxoverlay.html
.. _Benjamin: http://blogs.gnome.org/otte/2009/10/05/cairo-is-slow/
.. _Epiphany: http://live.gnome.org/Epiphany
.. _Gustavo: http://blog.kov.eti.br/
.. _Martin: http://abandonedwig.info/blog/