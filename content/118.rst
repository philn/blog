Gajim in your GNOME3 Shell
##########################
:date: 2011-04-08 09:57:51
:modified: 2011-04-08 09:57:51
:slug: 118

The IM integration in the GNOME3_ is interesting, but only for
the people using Empathy :) Because my personal preference goes
to another client called Gajim_ I wanted it to fit more in the
Shell. To reach this goal I worked on 2 small projects partly
during my Igalia_ hackfest hours and partly during my free time.

The first bit is a `Gajim plugin`_ integrating with the GNOME
Session-Manager and synchronizing your status in GNOME with your
Gajim status.

The second part is a `Shell extension`_ hooking to Gajim via
D-Bus to monitor chat notifications and allow chatting in the
Shell without directly using the Gajim window. This behaves
exactly as the Empathy integration.

There's still some work to do, like inhibiting notifications
coming from Gajim because currently when a incoming chat appears
the user gets a notification from the builtin Shell IM and a
notification from Gajim itself. A workaround is to disable some
notifications in the Gajim preferences.

As I needed some `new D-Bus API in Gajim`_ the Shell extension
requires Gajim-nightly >= 20110326.

Congrats to the GNOME Team for this awesome GNOME3 release and
happy chating :)



.. _GNOME3: http://gnome3.org
.. _Gajim: http://gajim.org
.. _Igalia: http://igalia.com
.. _Gajim plugin: http://trac-plugins.gajim.org/wiki/GnomeSessionManager
.. _Shell extension: http://git.gnome.org/browse/gnome-shell-extensions/tree/extensions/gajim
.. _new D-Bus API in Gajim: http://trac.gajim.org/ticket/6787