Firefox bookmarks remote backup
###############################
:date: 2004-07-20 00:13:07
:modified: 2005-05-30 22:10:38
:tags: Linux
:slug: 13

Tonight I upgraded my firefox debian package, re-installed my
favorites extensions (mozex,webDeveloper, ...) and `Bookmarks
Synchronizer`_. Well last time I tested it, there was support for FTP
only, but now the thing can handle HTTP(S) PUT :) That's was **THE**
missing feature, I love it now, it's a must-have !

.. _Bookmarks Synchronizer: http://update.mozilla.org/extensions/moreinfo.php?id=14&vid=15&category=Bookmarks
