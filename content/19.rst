it's all about music
####################
:date: 2004-10-04 09:16:08
:modified: 2005-05-30 22:09:11
:tags: Music
:slug: 19

Once upon something else than a time, there was a guy listening some music
(u2, an old one, the new one is not the u2 i like to hear) and thinking
about the thing behind this ... People ('pirates' comme on dit chez
tipiak(tm)) use to have lots, lots of music, usually in a big hdd ... so
what you want to listen is not as easy as saying 'i want a quarter pound w/
cheese' if you have hundreds of songs :) That's why some people design
software jukeboxes capable of handling all that stuff (partly) instead of
you. There are so many of these things just poking around (apt-cache says):

- juk : music organizer and player for KDE
- juke : A curses-based jukebox program
- madman : An advanced music manager application
- moosic : Daemon/client combo to easily queue music files for playing
- netjuke : Web-Based Audio Streaming Jukebox
- noatun : Media player (for video and audio)
- pytone : Music jukebox with advanced features for DJs and a text-mode user interface
- rhythmbox : music player and organizer for GNOME
- vux : A rating-based, random ogg and mp3 player
- zinf : Extensible, cross-platform audio player

And some others that seem to be popular like amaroK. I think the first one
was mp3sb written in p3rl (around 1998, jkx ?) and resPyre_ is its successor
(CORBA, Python). Despite the website is dead, there is some software and at
least one person using it. Pretty well for more than one year (excepted
during blind tests with friends :) Indeed there are some issues, buglets and
other goodies and/or features which makes it fun to use. i have one computer
(mostly) dedicated to music, running the server and few clients (LIRC, web)
making it easy to control by network or TV remote. 

The thing starts playing, using statistical calculations to find the
music i listen more, and sometimes trying to show me artists i don't
know / listen much. That's how i like to proceed most of the time, i'm
guided by resPyre music flow. When a song is played all along, its
score is incremented, when a song is being played to much times, it's
pushed away for a while. So i'm not always listening the same music i
love, and the system learn my tastes :) I guess other jukeboxes behave
in a similar way (if not, they should ;) The second must-have is
crossfading, i can't imagine a jukebox without this. And guess what
there must have some Python in the music-monster !!

Though jukeboxes seem well integrated in the desktop, i'm not using
any (Gnome, KDE, ...) so this is useless for me. I want only something
easy to access and control (xbindkeys for shortcuts and OSD for status
display are perfect for example).

Well i've tried many jukeboxes and most of them use OGG and MP3 tags
to organize the music. In my opinion, that sucks. Essential infos
(artist, album, song nb, song title) can easily be guessed if people
use a schema (BandName/Album/01 - Song.mp3) to store their music. Song
duration is guessable if you have the file (and using a good audio
lib), so why store it in the tag ? I believe 'Genre' tags suck
too. Music is a kind of art, and people can have different opinions
and labels to put on music. For instance Metallica (yeah the four
horsemen) are a 'hard-rock' band for many people. But they play
'slows' too. So, what's the balance ? And if a band can be categorized
under two different labels, that sucks too :) Nothing can be black and
white together.

.. _resPyre: http://respyre.org
