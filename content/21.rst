Python ALSA People out-there ?
##############################
:date: 2004-10-26 16:10:05
:modified: 2005-05-30 22:08:44
:tags: Python, Projects
:slug: 21

Python wrappers to ALSA_ seem to grow .. I've found 3 of them, including pyAlsa_ which is the first (correct me if i'm wrong). Anyway i believe there's a need of merge between all of them .. why duplicate stuff if we can find a way to build a good Python API to ALSA ?

So, interested people are cordially invited to join alsa-discuss mailing list which is intended for Python ALSA wrappers unification :) To join the list, simply send a mail to alsa-discuss-subscribe@lists.respyre.org. Archives are available at http://lists.respyre.org/archives/alsa-discuss/

.. _ALSA: http://alsa-project.org
.. _pyAlsa: http://respyre.org/pyalsa.html