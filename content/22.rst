Don't you have your XMLObject yet ? oh sweet heart :(
#####################################################
:date: 2004-10-29 13:35:00
:modified: 2005-05-30 22:08:30
:tags: Python, Projects, EaseXML
:slug: 22

Via DailyPython URL! ::

  Gre7g Luterman: XMLObject_ [XMLObject is a Python module that simplifies the
  handling of XML streams by converting the data into objects.]

That's kind of fun and reminds me the story about Python ALSA wrappers. People do things in their corner. And the final lambda user get lost in the jungle of XXX softwares doing about the same work. 

And in this case, especially using **the same name** for each project. So we have about three different XMLObject projects:

- `Zope XMLObject`_ .. for Zope,
- `Base-Art XMLObject`_,
- Last but not least `Gre7g XMLObject`_ ..
- Not to mention an XmlObject in Java :)

Are you confused ? Well i probably shouldn't have named my stuff like the Zoped one, i'm guilty here. Finally one more or one left, no difference. 

-- Reserve your XMLObject jungle map to your favorite receller :)

.. _XMLObject: http://xmlobject.sf.net
.. _Zope XMLObject: http://zope.org/Wikis/zope-xml/XMLObject
.. _Gre7g XMLObject: http://xmlobject.sf.net
.. _Base-Art XMLObject: http://xmlobject.base-art.net