About this blog
###############
:date: 2004-04-18 17:53:05
:modified: 2005-05-30 22:12:14
:tags: Python, Projects, Alinea
:slug: 4

Alinea is a blogware developped by:

- Jerome Kerdreux
- Ludovic Bellier
- Philippe Normand

It uses these technologies:

- Python : http://python.org
- Quixote2 : http://quixote.ca
- ReST : http://docutils.sf.net
- Cheetah : http://cheetahtemplate.org
- SQLObject : http://sqlobject.org

Current features are:

- user management
- articles / Sections
- hierarchical sections support
- threaded comments
- calendar
- Comments moderation
- spam protection
- admin view allowing full control of the beast
- article edition in ReST, HTML or simple text
- comment edition in ReST or simple text
- post preview

For more information, feel free to contact us.