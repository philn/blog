Remixes
#######
:date: 2005-03-18 16:14:28
:modified: 2005-05-30 22:04:38
:tags: Music
:slug: 43

Yesterday i watched Tracks_, a really good frecnh/dutch tv show about underground (music, techies, whatever) .. Yes, there still are some good shows on TV. TrashTV sucks !!

Anyway the show was about remixes. Some bands do all their business with that (they are probably paying lot of royalties for that). I particularly liked two of them :

- `Boss Hoss`_ : they do country with everything, even Britney !
- `Scala & Colacny Brothers`_: they do classic chorus with 90's (hard)rock. They made a very intersting album_ remixing  french various artists. I need that one !

Besides, Realplayer format simply sucks, Boss Hoss provides some MP3, thanks guys !

**2005/05/08 update**: i bought Scala & Colacny brothers **ResPyre** album few weeks ago. It's wonderful !

.. _Tracks: http://www.arte-tv.com/fr/art-musique/tracks/104524.html
.. _Boss Hoss: http://www.thebosshoss.de/enter.html
.. _Scala & Colacny Brothers: http://www.scalachor.de/
.. _album: http://www.scalachor.de/scala_respire.html