Vieilles Charrues 2005
######################
:date: 2005-07-25 18:15:29
:modified: 2005-09-17 22:46:12
:tags: Music
:slug: 52

De retour des labours_, un peu humides cette annÃ©e :) MalgrÃ© un
programme un peu moins intÃ©ressant que d'habitude, l'ambiance Ã©tait au
rendez-vous (et la boue aussi!). J'ai essayÃ© d'assister Ã  un maximum
de concerts dont voici quelques impressions.

Vendredi
--------

- `Hollywood Porn Stars`_, un groupe de rock belge bien sympa, Ã  surveiller :)
- An Pierle, je n'ai assistÃ© qu'Ã  la fin du concert, mais ca m'a bien dÃ©contractÃ© le dos
- New Order, la premiÃ¨re chanson m'a suffit 
- `Ghinzu`_, des guitares et une voix comme je les aime, une ambiance terrible. Bien quoi !
- Luke, impressionnant. Contrairement Ã  certains je ne pense pas que ce n'est que de la musique pour ado. Les textes riches d'idÃ©es sont agrÃ©mentÃ©s de mÃ©lodies entrainantes, ca dÃ©potte
- Deep Purple, la lÃ©gende du rock, mieux en vrai que dans le Live in Japan! 

Samedi
------

- Amadou et Mariam, des textes simples et bien rythmÃ©s, trÃ¨s sympa. Amadou est impressionnant Ã  la guitare !
- Mickey 3D, bonne prestation scÃ©nique, mais j'ai prÃ©fÃ©rÃ© leur prÃ©cÃ©dent passage aux charrues en 2003
- Louis Bertignac, je n'ai assistÃ© qu'Ã  la fin du concert, j'ai  un peu regrettÃ© d'en avoir ratÃ© le dÃ©but
- Iggy pop et les Stooges, l'Iguane Ã©gal Ã  lui-mÃªme. Je plains son cadreur car un iguane c'est pas Ã©vident Ã  filmer ;)

Dimanche
--------

- le spectacle de Annibal et ses Ã©lÃ©phants, hilarant, j'avais mal au ventre et j'en pleurait de rire. Coup de chapeau au fantastique acteur qu'est Jean-Louis, qui a rÃ©ussi Ã  dÃ©troner l'impitoyable Mr Cabot!- Nosfell, alors lui c'est un phÃ©nomÃ¨ne. Il commence chaque chanson par s'enregistrer un sample, et le repasse ensuite en boucle en ajoutant de nouveaux sons. De plus, il chante avec une langue bizarre venant de son enfance .. trÃ¨s original, ma foi ! (merci Guillaume pour les explications ;)
- Franz Ferdinand, top bon. J'attend leur prochain album avec impatience car ils ont jouÃ© pas mal de nouveaux morceaux bien sympathiques.
- Kool Shen, bof bof pas mon genre, mais c'etait mieux avant avec NTM ;)

Plus globalement, merci aux bÃ©nÃ©voles et Ã  l'Ã©quipe d'organisation
(sympa les copeaux de bois, pour combler un peu la boue). Ambiance de
fou au camping la nuit, j'ai pas dormi des masses, mais ma tente a
survÃ©cu !

A l'annÃ©e prochaine :)

.. _labours: http://vieillescharrues.asso.fr/festival/index.php
.. _Hollywood Porn Stars: http://www.hollywoodpornstars.be/
.. _Ghinzu: http://www.ghinzu.com/