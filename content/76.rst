New home, new country, same job
###############################
:date: 2006-09-01 20:51:39
:modified: 2006-09-01 23:45:25
:tags: Misc
:slug: 76

After nearly 6 months spent living in Lyon, I headed south once
more, to Barcelona :) I entered a new flat with 2 other frenchie
fellows, we'll be damn fine there!!

We still have some administrative paper work to do, but it can
wait, we did the essential ones.. The flat is a bit messy right
now, there are stuff to unpack lying around, and pieces to put
back together, but it will get better soon ;)

It seems there are many things to visit right there. Beach is
nice too, and mountains are not that far... So much to discover!

So, I will be in the same office as my co-workers at Fluendo,
still working on Elisa_. I think the project will evolve faster
and better in this new context. Talking with people face to face
is really better than IRC or video-conferencing; and by the way 
Fluendo folks are so fun to work with :)


.. _Elisa: http://fluendo.com/elisa/
