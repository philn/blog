Elisa 0.1.0
###########
:date: 2006-11-08 21:19:07
:modified: 2007-11-16 09:37:48
:tags: Python, Projects, Elisa
:slug: 79

It's been 4 months since the first alpha release of Elisa_, the Python/GStreamer/OpenGL MediaCenter developped by Fluendo_. It was time for a fresh release :)

Since GUADEC we've been working on separating the rendering, widget and animation frameworks in a separate project named Pigment_. It's hard to be really objective since I'm part of the development team but I have to say we really have spent some time discussing the architecture and how to implement it efficiently. Basically there's a C/GObject plugin based rendering engine (called render), a Python binding library (pyrender) and the pure Python widget framework relying on pyrender and including a damn powerful animation framework. Believe me it's really easy to get in touch with all this! For instance have a look at this `60-lines simple video player`_ and not speaking of the animation framework ;) We really believe other projects can benefit from Pigment and hope they will!

Speaking of Elisa, excepted for the skin, the global architecture didn't changed much, we still use Setuptools for the plugins framework and we are happy with it! Two new plugins appeared, one DAAP client and a Flickr pictures browser. I prototyped a Youtube plugin which might be part of next release which will be less than 4 months away from now :) We'll also start working on the PVR features really soon. Much bling to come!


.. _Fluendo: http://fluendo.com
.. _Elisa: http://fluendo.com/elisa/
.. _Pigment: https://core.fluendo.com/pigment/trac/
.. _60-lines simple video player: https://core.fluendo.com/pigment/trac/browser/trunk/examples/pigment/stream.py

