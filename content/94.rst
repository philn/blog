Coherence and Rhythmbox love
############################
:date: 2008-01-30 23:15:00
:modified: 2008-01-31 11:54:07
:tags: Python
:slug: 94

Some heads up and bling on Coherence_ front. Rhythmbox now acts as MediaServer, MediaClient and MediaRenderer thanks to Frank's great efforts. This is awesome :) See the demo video, it really blows you away:

.. raw:: html

  <object width="425" height="355"><param name="movie" value="http://www.youtube.com/v/N4dY2ByvOsU&rel=1"></param><param name="wmode" value="transparent"></param><embed src="http://www.youtube.com/v/N4dY2ByvOsU&rel=1" type="application/x-shockwave-flash" wmode="transparent" width="425" height="355"></embed></object>

And the Rhythmbox plugin is developed in Python

.. _coherence: https://coherence.beebits.net