Hi Planet GNOME!
################
:date: 2008-02-27 22:49:37
:modified: 2008-02-27 23:05:30
:tags: Misc
:slug: 97

Just a quick post to say hi to the `GNOME community`_. Thanks Jeff for adding me!

So I'm Philippe (philn on IRC), working for Fluendo on the Elisa_
MediaCenter, writing Python fluently during work hours for fun and
profit and off work, only for fun :)

I regularly write news about Elisa_, did some posts about Pigment_
and GStreamer_ too and a good bunch of posts related to various
hacks and ramblings about Python. 

So expect new posts about Pigment and Elisa towards better
integration in the GNOME desktop. I'll also try to blog about my
little growing experience in the GStreamer world :)

.. _GNOME community: http://planet.gnome.org
.. _Elisa: http://elisa.fluendo.com
.. _GStreamer: http://gstreamer.freedesktop.org
.. _Pigment: https://code.fluendo.com/pigment/trac/timeline