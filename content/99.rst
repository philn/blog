Coherence strikes back
######################
:date: 2008-05-03 17:15:38
:modified: 2008-05-03 17:16:09
:tags: Python
:slug: 99

With a release per month, Coherence_ is evolving rapidly, the
community is coming along, it's nice to see the IRC channel filling up
with developers and users. Recently Frank added a backend for
Tracker_, it's a bit rough on the edges currently and publishes only
the Music files over UPnP, but support for Videos and Pictures
publishing will come soon! In the future, every media file you'll have
on your desktop monitored by Tracker will be potentially available to
the other devices of your living network via UPnP.

One nice use-case is to use Rhythmbox on one computer to play the
media hosted on another desktop, transparently, with no headaches of
setting up a network share. Work is currently being done towards a
desktop applet called gcoherence_ meant to ease the configuration and
setup of Coherence:

.. image:: http://base-art.net/static/gcoherence.png 
   :target: gcoherence_

Another new feature of Coherence is a MediaServer backend for
Ampache_, a web-based audio jukebox system. I haven't tested that one
yet, but Ampache looks like a nice toy to play with in the near future
:)

Great stuff guys, keep up the good work :)

.. _Coherence: https://coherence.beebits.net/
.. _Tracker: http://www.gnome.org/projects/tracker/index.html
.. _gcoherence: http://29a.ch/gcoherence/
.. _Ampache: http://www.ampache.org/