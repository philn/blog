Title: GNOME Web Canary is back
Date: 2023-07-08 10:00:00
Tags: Projects, WebKit
Modified: 2023-07-08 17:30:00

This is a short PSA post announcing the return of the GNOME Web Canary builds.
Read on for the crunchy details.

A couple years ago I was blogging about the [GNOME Web Canary
flavor](https://base-art.net/Articles/introducing-the-gnome-web-canary-flavor/).
In summary this special build of GNOME Web provides a preview of the upcoming
version of the underlying WebKitGTK engine, it is potentially unstable, but
allows for testing features that have not shipped in a stable release yet.

Unfortunately, Canary broke right after GNOME Web switched to GTK4, because back
then the WebKit CI was missing build bots and infrastructure for hosting
WebKitGTK4 build artefacts. Recently, thanks to the efforts of my
[Igalia](https://igalia.com) colleagues, [Pablo
Abelenda](https://www.igalia.com/team/pabelenda), [Lauro
Moura](https://www.igalia.com/team/lmoura), [Diego
Pino](https://www.igalia.com/team/dpino) and [Carlos
López](https://www.igalia.com/team/clopez) the WebKit CI provides WebKitGTK4
build artefacts, hosted on a server kindly provided by Igalia.

<img src="{static}/png/Epiphany-Canary-GTK4.png"/>

The installation instructions are already mentioned in the introductory post but I'll just remind them again here:

```shell
flatpak --user remote-add --if-not-exists webkit https://software.igalia.com/flatpak-refs/webkit-sdk.flatpakrepo
flatpak --user install https://nightly.gnome.org/repo/appstream/org.gnome.Epiphany.Canary.flatpakref
```

**Update**:

If you installed the older version of Canary, pre-GTK4, you might see an error
related with an expired GPG key. This is due to how I update the WebKit runtime,
and I'll try to avoid it in future updates. For the time being, you can remove
the flatpak remote and re-add it:

```shell
flatpak --user remote-delete webkit
flatpak --user remote-add webkit https://software.igalia.com/flatpak-refs/webkit-sdk.flatpakrepo
```

That's all folks, happy hacking and happy testing.
