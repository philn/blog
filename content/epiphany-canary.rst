Introducing the GNOME Web Canary flavor
#######################################
:tags: Projects, WebKit
:date: 2021-08-02 14:00:00
:modified: 2021-08-02 19:15:00

Today I am happy to unveil GNOME Web Canary which aims to provide bleeding edge,
most likely very unstable builds of Epiphany, depending on daily builds of the
WebKitGTK development version. Read on to know more about this.

Until recently the `GNOME Web browser`_ was available for end-users in two
flavors. The primary, stable release provides the vanilla experience of the
upstream Web browser. It is shipped as part of the GNOME release cycle and in
distros. The second flavor, called `Tech Preview`_, is oriented towards early
testers of GNOME Web. It is available as a Flatpak, included in the GNOME
nightly repo. The builds represent the current state of the GNOME Web master
branch, the WebKitGTK_ version it links to is the one provided by the GNOME
nightly runtime.

Tech Preview is great for users testing the latest development of GNOME Web, but
what if you want to test features that are not yet shipped in *any* WebKitGTK
version? Or what if you are GNOME Web developer and you want to implement new
features on Web that depend on API that was not released yet in WebKitGTK?

Historically, the answer was simply "you can `build WebKitGTK yourself`_".
However, this requires some knowledge and a good build machine (or a lot of
patience). Even as WebKit developer builds have become easier to produce thanks
to the Flatpak SDK we provide, you would still need to somehow make Epiphany
detect your local build of WebKit. Other browsers offer nightly or "Canary"
builds which don't have such requirements. This is exactly what Epiphany Canary
aims to do! Without building WebKit yourself!

A brief interlude about the term: Canary typically refers to highly unstable
builds of a project, they are named after `Sentinel species`_. Canary birds were
taken into mines to warn coal miners of carbon monoxide presence. For instance
`Chrome has been providing Canary builds`_ of its browser for a long time. These
builds are useful because they allow early testing, by end-users. Hence
potentially early detection of bugs that might not have been detected by the
usual automated test harness that buildbots and CI systems run.

To similar ends, a new build profile and icon were added in Epiphany, along with
a new Flatpak manifest. Everything is now nicely integrated in the Epiphany
project CI. WebKit builds are already done for every upstream commit using the
`WebKit Buildbot`_. As those builds are made with the WebKit Flatpak SDK, they
can be reused elsewhere (x86_64 is the only arch supported for now) as long as
the WebKit Flatpak platform runtime is being used as well. Build artifacts are
saved, compressed, and `uploaded to a web server`_ kindly hosted and provided by
Igalia_. The GNOME Web CI now has a new job, called ``canary``, that generates a
build manifest that installs WebKitGTK build artifacts in the build sandbox,
that can be detected during the Epiphany Flatpak build. The resulting Flatpak
bundle can be downloaded and locally installed. The runtime environment is the
one provided by the WebKit SDK though, so not exactly the same as the one
provided by GNOME Nightly.

Back to the two main use-cases, and who would want to use this:

- You are a GNOME Web developer looking for CI coverage of some shiny new
  WebKitGTK API you want to use from GNOME Web. Every new merge request on the
  GNOME Web Gitlab repo now produces installable Canary bundles, that can be
  used to test the code changes being submitted for review. This bundle is not
  automatically updated though, it's good only for one-off testing.

- You are an early tester of GNOME Web, looking for bleeding edge version of
  both GNOME Web and WebKitGTK. You can `install Canary`_ using the provided
  Flatpakref. Every commit on the GNOME Web master branch produces an update of
  Canary, that users can get through the usual ``flatpak update`` or through their
  flatpak-enabled app-store.

**Update:**

Due to an issue in the Flatpakref file, the WebKit SDK flatpak remote
is not automatically added during the installation of GNOME Web Canary. So it
needs to be manually added before attempting to install the flatpakref:

::

  $ flatpak --user remote-add --if-not-exists webkit https://software.igalia.com/flatpak-refs/webkit-sdk.flatpakrepo
  $ flatpak --user install https://nightly.gnome.org/repo/appstream/org.gnome.Epiphany.Canary.flatpakref

As you can see in the screenshot below, the GNOME Web branding is clearly
modified compared to the other flavors of the application. The updated logo,
kindly provided by Tobias Bernard, has some yellow tones and the Tech Preview
stripes. Also the careful reader will notice the reported WebKitGTK version in
the screenshot is a development build of SVN revision r280382. Users are
strongly advised to add this information to bug reports.

.. raw:: html

  <img src="{static}/png/Epiphany-Canary.png"/>

As WebKit developers we are always interested in getting users' feedback. I hope
this new flavor of GNOME Web will be useful for both GNOME and WebKitGTK
communities. Many thanks to Igalia_ for sponsoring WebKitGTK build artifacts
hosting and some of the work time I spent on this side project. Also thanks to
Michael Catanzaro, Alexander Mikhaylenko and Jordan Petridis for the reviews in
Gitlab.

.. _GNOME Web browser: https://wiki.gnome.org/Apps/Web
.. _Tech Preview: https://blogs.gnome.org/mcatanzaro/2018/01/24/announcing-epiphany-technology-preview/
.. _WebKitGTK: https://webkitgtk.org
.. _build WebKitGTK yourself: https://trac.webkit.org/wiki/BuildingGtk
.. _Sentinel species: https://en.wikipedia.org/wiki/Sentinel_species
.. _Chrome has been providing Canary builds: https://www.google.com/chrome/canary/
.. _WebKit Buildbot: https://build.webkit.org/
.. _uploaded to a web server: https://webkitgtk-release.igalia.com/built-products/
.. _Igalia: https://igalia.com
.. _install Canary: https://nightly.gnome.org/repo/appstream/org.gnome.Epiphany.Canary.flatpakref
