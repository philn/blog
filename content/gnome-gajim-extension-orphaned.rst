The GNOME-Shell Gajim extension maintenance
###########################################
:tags: Projects

Back in January 2011 I wrote a GNOME-Shell extension_ allowing Gajim users to
carry on with their chats using the Empathy infrastructure and UI present in the
Shell. For some time the extension was also part of the official
gnome-shell-extensions module and then I had to move it to Github as a
`standalone extension`_. Sadly I stopped using Gajim a few years ago and my
interest in maintaining this extension has decreased quite a lot.

I don't know if this extension is actively used by anyone beyond the few bugs
reported in Github, so this is a call for help. If anyone still uses this
extension and wants it supported in future versions of GNOME-Shell, please send
me a mail so I can transfer ownership of the Github repository and see what I
can do for the extensions.gnome.org page as well.

(Huh, also. Hi blogosphere again! My last post was in 2014 it seems :))

.. _extension: https://extensions.gnome.org/extension/565/gajim-im-integration/
.. _standalone extension: https://github.com/philn/gnome-shell-gajim-extension
