GStreamer's playbin3 overview for application developers
########################################################
:tags: Projects, GStreamer

Multimedia applications based on GStreamer usually handle playback with the
playbin_ element. I recently added support for playbin3_ in WebKit. This post
aims to document the changes needed on application side to support this new
generation flavour of playbin.

So, first of, why is it named playbin3 anyway? The GStreamer 0.10.x series had a
playbin element but a first rewrite (playbin2) made it obsolete in the GStreamer
1.x series. So playbin2 was renamed to playbin. That's why a second rewrite is
nicknamed playbin3, I suppose :)

Why should you care about playbin3? Playbin3 (and the elements it's using
internally: parsebin, decodebin3, uridecodebin3 among others) is the result of a
deep re-design of playbin2 (along with decodebin2 and uridecodebin) to better
support:

- gapless playback
- audio cross-fading support (not yet implemented)
- adaptive streaming
- reduced CPU, memory and I/O resource usage
- faster stream switching and full control over the stream selection process

This work was carried on mostly by Edward Hervey, he presented his work in
detail at 3 GStreamer conferences. If you want to learn more about this and the
internals of playbin3 make sure to watch his awesome presentations at the `2015
gst-conf`_, `2016 gst-conf`_ and `2017 gst-conf`_.

Playbin3 was added in GStreamer 1.10. It is still considered experimental but in
my experience it works already very well. Just keep in mind you should use at
least the latest GStreamer 1.12 (or even the upcoming 1.14) release before
reporting any issue in Bugzilla. Playbin3 is not a drop-in replacement for
playbin, both elements share only a sub-set of GObject properties and signals.
However, if you don't want to modify your application source code just yet, it's
very easy to try playbin3 anyway:

::

   $ USE_PLAYBIN3=1 my-playbin-based-app

Setting the `USE_PLAYBIN` environment variable enables a code path inside the
GStreamer playback plugin which swaps the playbin element for the playbin3
element. This trick provides a glance to the playbin3 element for the most lazy
people :) The problem is that depending on your use of playbin, you might get
runtime warnings, here's an example with the Totem player:

::

   $ USE_PLAYBIN3=1 totem ~/Videos/Agent327.mp4
   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'video-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'audio-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'text-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'video-tags-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'audio-tags-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

   (totem:22617): GLib-GObject-WARNING **: ../../../../gobject/gsignal.c:2523: signal 'text-tags-changed' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'
   sys:1: Warning: g_object_get_is_valid_property: object class 'GstPlayBin3' has no property named 'n-audio'
   sys:1: Warning: g_object_get_is_valid_property: object class 'GstPlayBin3' has no property named 'n-text'
   sys:1: Warning: ../../../../gobject/gsignal.c:3492: signal name 'get-video-pad' is invalid for instance '0x556db67f3170' of type 'GstPlayBin3'

As mentioned previously, playbin and playbin3 don't share the same set of
GObject properties and signals, so some changes in your application are required
in order to use playbin3.

If your application is based on the GstPlayer_ library then you should set the
`GST_PLAYER_USE_PLAYBIN3` environment variable. GstPlayer already handles both
playbin and playbin3, so no changes needed in your application if you use
GstPlayer!

Ok, so what if your application relies directly on playbin? Some
changes are needed! If you previously used playbin stream selection
properties and signals, you will now need to handle the GstStream and
GstStreamCollection APIs. Playbin3 will emit a `stream collection
message`_ on the bus, this is very nice because the collection
includes information (metadata!) about the streams (or tracks) the
media asset contains. In playbin this was handled with a bunch of
signals (audio-tags-changed, audio-changed, etc), properties (n-audio,
n-video, etc) and action signals (get-audio-tags, get-audio-pad, etc).
The new `GstStream API`_ provides a centralized and
non-playbin-specific access point for all these informations. To
select streams with playbin3 you now need to send a `select_streams
event`_ so that the demuxer can know exactly which streams should be
exposed to downstream elements. That means potentially improved
performance! Once playbin3 completed the stream selection it will emit
a `streams selected message`_, the application should handle this
message and potentially update its internal state about the selected
streams. This is also the best moment to update your UI regarding the
selected streams (like audio track language, video track dimensions,
etc).

Another small difference between playbin and playbin3 is about the source
element setup. In playbin there is a `source` read-only GObject property and a
`source-setup` GObject signal. In playbin3 only the latter is available, so your
application should rely on `source-setup` instead of the `notify::source`
GObject signal.

The `gst-play-1.0`_ playback utility program already supports playbin3 so it
provides a good source of inspiration if you consider porting your application
to playbin3. As mentioned at the beginning of this post, WebKit also now
supports playbin3, however it needs to be enabled at build time using the CMake
`-DUSE_GSTREAMER_PLAYBIN3=ON` option. This feature is not part of the WebKitGTK+
2.20 series but should be shipped in 2.22. As a final note I wanted to
acknowledge my favorite worker-owned coop Igalia_ for allowing me to work on
this WebKit feature and also our friends over at Centricular_ for all the
quality work on playbin3.


.. _playbin: https://gstreamer.freedesktop.org/documentation/tutorials/playback/playbin-usage.html
.. _playbin3: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-base-plugins/html/gst-plugins-base-plugins-playbin3.html
.. _GstPlayer: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gst-plugins-bad-libs/html/GstPlayer.html
.. _2015 gst-conf: https://gstconf.ubicast.tv/videos/decodebin3-or-dealing-with-modern-playback-use-cases/
.. _2016 gst-conf: https://gstconf.ubicast.tv/videos/the-new-gststream-api-design-and-usage/
.. _2017 gst-conf: https://gstconf.ubicast.tv/videos/lightning-talks/#start=1418&autoplay&timeline
.. _stream collection message: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstMessage.html#GST-MESSAGE-STREAM-COLLECTION:CAPS
.. _GstStream API: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/gstreamer-GstStream.html
.. _select_streams event: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstEvent.html#gst-event-new-select-streams
.. _streams selected message: https://gstreamer.freedesktop.org/data/doc/gstreamer/head/gstreamer/html/GstMessage.html#GST-MESSAGE-STREAMS-SELECTED:CAPS
.. _gst-play-1.0: https://cgit.freedesktop.org/gstreamer/gst-plugins-base/tree/tools/gst-play.c
.. _Igalia: https://igalia.com
.. _Centricular: https://centricular.com
