HTML overlays with GstWPE, the demo
###################################
:tags: Projects, GStreamer, WebKit
:date: 2019-12-08 15:00:00
:modified: 2019-12-08 15:00:00

Once again this year I attended the `GStreamer conference`_ and just before
that, `Embedded Linux conference Europe`_ which took place in Lyon (France).
Both events were a good opportunity to demo one of the use-cases I have in mind
for GstWPE_, HTML overlays!

As we, at Igalia_, usually have a booth at ELC, I thought a GstWPE demo would be
nice to have so we can show it there. The demo is a rather simple GTK
application presenting a live preview of the webcam video capture with an HTML
overlay blended in. The HTML and CSS can be modified using the embedded text
editor and the overlay will be updated accordingly. The final video stream can
even be streamed over RTMP to the main streaming platforms (Twitch, Youtube,
Mixer)! Here is a screenshot:

.. raw:: html

  <img src="{filename}/png/gst-wpe-broadcast-demo-screenshot.png"/>

The code of the demo is available on `Igalia's GitHub`_. Interested people
should be able to try it as well, the app is packaged as a Flatpak. See the
instructions in the GitHub repo for more details.

Having this demo running on our booth greatly helped us to explain GstWPE and
how it can be used in real-life GStreamer_ applications. Combining the
flexibility of the Multimedia framework with the wide (wild) features of the Web
Platform will for sure increase the synergy and foster collaboration!

As a reminder, GstWPE is available in GStreamer_ since the 1.16 version. On the
roadmap for the mid-term I plan to add Audio support, thus allowing even better
integration between WPEWebKit and GStreamer. Imagine injecting PCM audio coming
from WPEWebKit into an audio mixer in your GStreamer-based application! Stay
tuned.


.. _GStreamer conference: https://gstreamer.freedesktop.org/conference/2019/
.. _Embedded Linux conference Europe: https://events19.linuxfoundation.org/events/embedded-linux-conference-europe-2019/
.. _GstWPE: https://base-art.net/Articles/web-overlay-in-gstreamer-with-wpewebkit/
.. _Igalia: https://igalia.com
.. _Igalia's GitHub: https://github.com/Igalia/gst-wpe-broadcast-demo
.. _GStreamer: https://gstreamer.freedesktop.org/
