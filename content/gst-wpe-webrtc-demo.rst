Web-augmented graphics overlay broadcasting with WPE and GStreamer
##################################################################
:tags: Projects, GStreamer, WebKit
:date: 2020-07-02 15:00:00
:modified: 2020-07-02 15:00:00

Graphics overlays are everywhere nowadays in the live video broadcasting
industry. In this post I introduce a new demo relying on GStreamer_ and
WPEWebKit_ to deliver low-latency web-augmented video broadcasts.

Readers of this blog might remember a few posts about WPEWebKit_ and a
GStreamer_ element we at Igalia_ worked on. In december 2018 I `introduced
GstWPE`_ and a few months later blogged about a `proof-of-concept application`_
I wrote for it. So, learning from this first iteration, I wrote another demo!

The first demo was already quite cool, but had a few down-sides:

1. It works only on desktop (running in a Wayland compositor). The Wayland
   compositor dependency can be a burden in some cases. Ideally we could
   imaginge GstWPE applications running "in the cloud", on machines without GPU,
   bare metal.

2. While it was cool to stream to Twitch, Youtube and the like, these platforms
   currently can ingest only RTMP streams. That means the latency introduced can
   be quite significant, depending on the network conditions of course, but even
   in ideal conditions the latency was between one and 2 seconds. This is not
   great, in the world we live in.

To address the first point, WPE founding engineer, `Žan Doberšek`_ enabled
software rasterizing support in WPE and its `FDO backend`_. This is great because
it allows WPE to run on machines without GPU (like continuous integration
builders, test bots) but also "in the cloud" where machines with GPU are less
affordable than bare metal! Following up, I enabled this feature in GstWPE. The
source element caps template now has `video/x-raw`, in addition to
`video/x-raw(memory:GLMemory)`. To force swrast, you need to set the
`LIBGL_ALWAYS_SOFTWARE=true` environment variable. The downside of swrast is
that you need a good CPU. Of course it depends on the video resolution and
framerate you want to target.

On the latency front, I decided to switch from RTMP to WebRTC! This W3C spec
isn't only about video chat! With WebRTC, sub-second live one-to-many
broadcasting can be achieved, without much efforts, given you have a good SFU.
For this demo I chose Janus_, because its APIs are well documented, and it's a
cool project! I'm not sure it would scale very well in large deployments, but
for my modest use-case, it fits very well.

Janus has a plugin called video-room_ which allows multiple participants to
chat. But then imagine a participant only publishing its video stream and
multiple "clients" connecting to that room, without sharing any video or audio
stream, one-to-many broadcasting. As it turns out, GStreamer_ applications can
already connect to this video-room plugin using GstWebRTC_! A demo was developed
by tobiasfriden_ and saket424_ in Python, it recently moved to the gst-examples_
repository. As I kind of prefer to use Rust nowadays (whenever I can anyway) I
ported this demo to Rust, it was upstreamed in gst-examples_ as well. This
specific demo streams the video test pattern to a Janus instance.

Adapting this Janus demo was then quite trivial. By relying on a similar video
mixer approach I used for the first GstWPE demo, I had a GstWPE-powered WebView
streaming to Janus.

The next step was the actual graphics overlays infrastructure. In the first
GstWPE demo I had a basic GTK UI allowing to edit the overlays on-the-fly. This
can't be used for this new demo, because I wanted to use it headless. After
doing some research I found a really nice NodeJS app on Github, it was developed
by `Luke Moscrop`_, who's actually one of the main developers of the `Brave
BBC`_ project. The `Roses CasparCG Graphics`_ was developed in the context of
the `Lancaster University Students' Union TV Station`_, this app starts a
web-server on port 3000 with two main entry points:

- An admin web-UI (in `/admin/` allowing to create and manage overlays, like sports score
  boards, info banners, and so on.
- The target overlay page (in the root location of the server), which is a
  web-page without predetermined background, displaying the overlays with HTML,
  CSS and JS. This web-page is meant to be fed to CasparCG_ (or GstWPE :))

After making a few tweaks in this NodeJS app, I can now:

1. Start the NodeJS app, load the admin UI in a browser and enable some overlays
2. Start my native Rust GStreamer/WPE application, which:

   - connects to the overlay web-server
   - mixes a live video source (webcam for instances) with the WPE-powered overlay
   - encodes the video stream to H.264, VP8 or VP9
   - sends the encoded RTP stream using WebRTC to a Janus server

3. Let "consumer" clients connect to Janus with their browser, in order to see
   the resulting live broadcast.

.. raw:: html

   <iframe width="560" height="315" src="https://www.youtube.com/embed/QNZJYOuVGiE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

(If the video doesn't display, here is the `Youtube link`_.)

This is pretty cool and fun, as my colleague `Brian Kardell`_ mentions in the
video. Working on this new version gave me more ideas for the next one. And very
recently the `audio rendering protocol`_ was merged in WPEBackend-FDO! That
means even more use-cases are now unlocked for GstWPE.

This demo's source code is hosted on Github_. Feel free to open issues there, I
am always interested in getting feedback, good or bad!

GstWPE is maintained upstream in GStreamer_ and relies heavily on WPEWebKit_ and
its `FDO backend`_. Don't hesitate to `contact us`_ if you have specific
requirements or issues with these projects :)

.. _GStreamer: https://gstreamer.freedesktop.org
.. _WPEWebKit: https://wpewebkit.org
.. _introduced GstWPE: https://base-art.net/Articles/web-overlay-in-gstreamer-with-wpewebkit/
.. _proof-of-concept application: https://base-art.net/Articles/html-overlays-with-gstwpe-the-demo/
.. _Žan Doberšek: https://blogs.igalia.com/zdobersek/
.. _FDO backend: https://github.com/igalia/wpebackend-fdo
.. _Janus: https://janus.conf.meetecho.com
.. _video-room: https://janus.conf.meetecho.com/docs/videoroom.html
.. _GstWebRTC: https://gstreamer.freedesktop.org/documentation/webrtc/index.html?gi-language=c#webrtcbin-page
.. _gst-examples: https://gitlab.freedesktop.org/gstreamer/gst-examples
.. _tobiasfriden: https://github.com/tobiasfriden
.. _saket424: https://github.com/saket424
.. _Luke Moscrop: https://github.com/moschopsuk
.. _Brave BBC: https://github.com/bbc/brave
.. _Roses CasparCG Graphics: https://github.com/moschopsuk/Roses-2015-CasparCG-Graphics
.. _Lancaster University Students' Union TV Station: https://www.la1tv.co.uk
.. _CasparCG: https://casparcg.com
.. _Brian Kardell: https://bkardell.com
.. _audio rendering protocol: https://github.com/Igalia/WPEBackend-fdo/commit/fd14bac16e465083b755675c441e19cc60db04b3
.. _Youtube link: https://youtu.be/QNZJYOuVGiE
.. _contact us: https://www.igalia.com/contact/
.. _Github: https://github.com/Igalia/gst-wpe-webrtc-demo
.. _Igalia: https://igalia.com
