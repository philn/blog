=========================================================
Review of the Igalia Multimedia team Activities (2019/H1)
=========================================================
:tags: Projects, WebKit, GStreamer
:date: 2019-08-05 15:30
:modified: 2019-08-05 15:30

This blog post takes a look back at the various Multimedia-related tasks the
`Igalia Multimedia team`_ was involved in during the first half of 2019.

GStreamer Editing Services
==========================

Thibault added support for the OpenTimelineIO_ open format for editorial
timeline information. Having many editorial timeline information formats
supported by OpenTimelineIO reduces vendor lock-in in the tools used by video artists in
post-production studios. For instance a movie project edited in Final Cut Pro
can now be easily reimported in the Pitivi_ free and open-source video editor.
This accomplishment was made possible by implementing an OpenTimelineIO GES
adapter_ and formatter_, both upstreamed respectively in OpenTimelineIO and GES by Thibault.

.. raw:: html

  <img src="{filename}/png/FcpAndPitivi.png"/>

Another important feature for non-linear video editors is nested timeline
support. It allows teams to decouple big editing projects in smaller chunks that
can later on be assembled for the final product. Another use-case is about
pre-filling the timeline with boilerplate scenes, so that an initial version of
the movie can be assembled before all teams involved in the project have
provided the final content. To support this, Thibault implemented a `GES demuxer`_ which transparently enables
playback support for GES files (through file://path/to/file.xges URIs) in any GStreamer-based
media player.

As if this wasn't impressive enough yet, Thibault greatly improved the GES
unit-tests, fixing a lot of memory leaks, race conditions and generally
improving the reliability of the test suite. This is very important because the
Gitlab continuous integration now executes the tests harness for every submitted
merge request.

For more information about this, the curious readers can dive in `Thibault's blog post`_.
Thibault was invited to talk about these on-going efforts at SIGGRAPH_ during
the `OpenTimelineIO BOF`_.

Finally, Thibault is mentoring `Swayamjeet Swain`_ as part of the GSoC_ program,
the project is about adding nested timeline support in Pitivi_.

GStreamer VA-API
================

Víctor performed a good number of code reviews for GStreamer-VAAPI contributors,
he has also started investigating GStreamer-VAAPI bugs specific to the AMDGPU
Gallium driver, in order to improve the support of AMD hardware in multimedia
applications.

As part of the on-going GStreamer community efforts to improve continuous
integration (CI) in the project, we purchased Intel and AMD powered devices
aimed to run validation tests. Running CI on real end-user hardware will help
ensure regressions remain under control.

Servo and GStreamer-rs
======================

As part of our on-going involvement in the Servo_ Mozilla project, Víctor_ has
enabled zero-copy video rendering support in the GStreamer-based `Servo-media`_
crate, along with `bug fixes in Glutin`_ and finally in `Servo itself`_.

A prior requirement for this major milestone was to enable gst-gl in the
GStreamer Rust bindings and after around 20 patches were merged in the
repository, we are pleased to announce Linux and Android platforms are
supported. Windows and macOS platforms will also be supported, soon.

The following screencast shows how hardware-accelerated video rendering performs
on Víctor's laptop:

.. raw:: html

   <video controls src="https://s3.amazonaws.com/media-p.slid.es/videos/105177/rzteE40V/hwacceleration.mp4"/>

Víctor also delivered a talk at the MadRust_ meetup, about GStreamer-rs, the
`slides are available`_ on his website.

WebKit's Media Source Extensions
================================

As part of our on-going collaboration with various device manufacturers, Alicia_
and Enrique_ validated the `Youtube TV MSE 2019 test suite`_ in WPEWebKit-based
products.

Alicia developed a new GstValidate plugin to improve GStreamer pipelines
testing, called validateflow_. Make sure to read her `blog post about it`_!.

In her quest to further improve MSE support, especially seek support, Alicia
rewrote the GStreamer source element we use in WebKit for MSE playback. The code
`review is on-going in Bugzilla`_ and on track for inclusion in WPEWebKit and
WebKitGTK 2.26. This new design of the MSE source element requires the playbin3
GStreamer element and at least GStreamer 1.16. Another feature we plan to work
on in the near future is multi-track support; stay tuned!

WebKit WebRTC
=============

We announced `LibWebRTC support for WPEWebKit and WebKitGTK`_ one year ago.
Since then, Thibault has been implementing new features and fixing bugs in the
backend. Bridging between the WebAudio and WebRTC backend was implemented,
allowing tight integration of WebAudio and WebRTC web apps. More WebKit WebRTC
layout tests were unskipped in the buildbots, allowing better tracking of
regressions.

Thibault also fixed various performance issues on Raspberry Pi platforms during
apprtc_ video-calls. As part of this effort he upstreamed a
GstUVCH264DeviceProvider_ in GStreamer, allowing applications to use
already-encoded H264 streams from webcams that provide it, thus removing the
need for applications to encode raw video streams.

Additionally, Thibault upstreamed a `device provider for ALSA`_ in GStreamer,
allowing applications to probe for Microphones and speakers supported through
the ALSA kernel driver.

Finally, Thibault tweaked the GStreamer encoders used by the WebKit LibWebRTC
backend, in order to match the behavior of the Apple implementation and also
fixing a few performance and rendering issues on the way.

WebKit GStreamer Multimedia maintenance
=======================================

The whole team is always keeping an eye on WebKit's Bugzilla, watching out for
multimedia-related bugs reported by the community members. Charlie_ recently
fixed a few annoying volume_ bugs_ along with an issue related with youtube_.

I rewrote the WebKitWebSrc_ GStreamer source element we use in WebKit to
download HTTP(S) media resources and feed the data to the playback pipeline.
This new element is now based on the GStreamer pushsrc base class, instead of
appsrc. A few seek-related issues were fixed on the way but unfortunately some
regressions also slipped in; those should all be fixed by now and shipped in
WPEWebKit/WebKitGTK 2.26.

An initial version of the MediaCapabilities_ backend_ was also upstreamed in
WebKit. It allows web-apps (such as Youtube TV) to probe the user-agent for
media decoding and encoding capabilities. The GStreamer backend relies on the
GStreamer plugin registry to provide accurate information about the supported
codecs and containers. `H264 AVC1 profile and level`_ information are also
probed.

WPEQt
=====

Well, this isn't directly related with multimedia, but I finally announced the
initial release of WPEQt_. Any feedback is welcome, QtWebKit has phased out and
if anyone out there relies on it for web-apps embedded in native QML apps, now
is the time to try WPEQt!

.. _Igalia Multimedia team: https://www.igalia.com/technology/multimedia
.. _OpenTimelineIO: https://github.com/PixarAnimationStudios/OpenTimelineIO
.. _Pitivi: https://pitivi.org
.. _adapter: https://github.com/PixarAnimationStudios/OpenTimelineIO/pull/412
.. _formatter: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/merge_requests/67
.. _GES demuxer: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/blob/master/plugins/ges/gesdemux.c
.. _GES source element: https://gitlab.freedesktop.org/gstreamer/gst-editing-services/blob/master/plugins/ges/gessrc.c
.. _Thibault's blog post: https://blogs.gnome.org/tsaunier/2019/04/22/gstreamer-editing-services-opentimelineio-support/
.. _GSoC: https://summerofcode.withgoogle.com/
.. _Swayamjeet Swain: https://swaynethoughts.wordpress.com/
.. _SIGGRAPH: https://s2019.siggraph.org/
.. _OpenTimelineIO BOF: https://s2019.siggraph.org/presentation/?id=bof_124&sess=sess314
.. _Servo: https://servo.org/
.. _Víctor: https://blogs.igalia.com/vjaquez
.. _Servo-media: https://github.com/servo/media
.. _bug fixes in Glutin: https://github.com/rust-windowing/glutin/pull/1082
.. _Servo itself: https://github.com/servo/servo/pull/23483
.. _MadRust: https://www.meetup.com/MadRust/events/259059099/
.. _slides are available: https://people.igalia.com/vjaquez/talks/madrust2019/
.. _Enrique: https://www.igalia.com/igalian/eocanha
.. _Alicia: https://www.igalia.com/igalian/aboya
.. _Youtube TV MSE 2019 test suite: https://ytlr-cert.appspot.com/2019/main.html
.. _validateflow: https://gstreamer.freedesktop.org/documentation/gst-devtools/plugins/validateflow.html?gi-language=c
.. _blog post about it: https://blogs.igalia.com/aboya/2019/05/14/validateflow-a-new-tool-to-test-gstreamer-pipelines/
.. _review is on-going in Bugzilla: https://bugs.webkit.org/show_bug.cgi?id=199719
.. _LibWebRTC support for WPEWebKit and WebKitGTK: https://blogs.gnome.org/tsaunier/2018/07/31/webkitgtk-and-wpe-gains-webrtc-support-back/
.. _apprtc: https://appr.tc/
.. _GstUVCH264DeviceProvider: https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad/commit/ee108d0ed20c376b5871b40108ba1cd54156baf9
.. _device provider for ALSA: https://gitlab.freedesktop.org/gstreamer/gst-plugins-base/commit/ac5d0f7da688c1010ab2656f32c40e854e5b8bf2
.. _Charlie: https://www.igalia.com/igalian/cturner
.. _volume: https://bugs.webkit.org/show_bug.cgi?id=197358
.. _bugs: https://bugs.webkit.org/show_bug.cgi?id=199505
.. _youtube: https://bugs.webkit.org/show_bug.cgi?id=197355
.. _WebKitWebSrc: https://trac.webkit.org/changeset/243058/webkit
.. _MediaCapabilities: https://w3c.github.io/media-capabilities/
.. _backend: https://bugs.webkit.org/show_bug.cgi?id=191191
.. _H264 AVC1 profile and level: https://bugs.webkit.org/show_bug.cgi?id=198569
.. _WPEQt: https://base-art.net/Articles/introducing-wpeqt-a-wpe-api-for-qt5/
