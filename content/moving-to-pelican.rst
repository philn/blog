Moving to Pelican
#################
:tags: Misc

Time for a change! Almost 10 years ago I was starting to hack on a
Blog engine with two friends, it was called Alinea_ and it powered
this website for a long time. Back then hacking on your own Blog
engine was the pre-requirement to host your blog :) But nowadays
people just use Wordpress or similar platforms, if they still have a
blog at all. Alinea fell into oblivion as I didn't have time and
motivation to maintain it.

Moving to Pelican was quite easy, since I've been writing content in
ReST on the previous blog I only had to pull data from the database
and hacked pelican_import.py a bit :)

Now that this website looks almost modern I'll hopefully start to blog
again, expect at least news about the WebKit_ work I still enjoy doing
at Igalia_.

.. _Alinea: http://github.com/philn/alinea
.. _WebKit: http://webkit.org
.. _Igalia: http://igalia.com
