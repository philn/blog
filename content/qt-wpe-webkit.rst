====================================
Introducing WPEQt, a WPE API for Qt5
====================================
:tags: Projects, WebKit
:date: 2019-04-08 12:20
:modified: 2019-04-08 12:20

WPEQt provides a QML plugin implementing an API very similar to the `QWebView
API`_. This blog post explains the rationale behind this new project aimed for
QtWebKit users.

Qt5 already provides multiple WebView APIs, one based on QtWebKit (deprecated)
and one based on QWebEngine (aka Chromium). WPEQt aims to provide a viable
alternative to the former. QtWebKit is being retired and has by now lagged a lot
behind upstream WebKit in terms of features and security fixes. WPEQt can also
be considered as an alternative to QWebEngine but bear in mind the underlying
Chromium web-engine doesn't support the same HTML5 features as WebKit.

WPEQt is included in WPEWebKit, starting from the 2.24 series. Bugs should be
reported in WebKit's Bugzilla. WPEQt's code is published under the same licenses
as WPEWebKit, the LGPL2 and BSD.

At Igalia_ we have compared `WPEQt and QtWebKit`_ using the `BrowserBench tests`_.
The JetStream1.1 results show that WPEQt completes all the tests twice
as fast as QtWebKit. The Speedometer benchmark doesn't even finish due to a
crash in the QtWebKit DFG JIT. Although the memory consumption looks similar in
both engines, the upstream WPEQt engine is well maintained and includes security
bug-fixes. Another advantage of WPEQt compared to QtWebKit is that its
multimedia support is much stronger, with specs such as MSE, EME and
media-capabilities being covered. WebRTC support is coming along as well!

So to everybody still stuck with QtWebKit in their apps and not yet ready (or
reluctant) to migrate to QtWebEngine, please have a look at WPEQt! The
remaining of this post explains how to build it and test it.

.. _WPEQt and QtWebKit: https://people.igalia.com/pnormand/wpe-qt-webkit-comparison/
.. _BrowserBench tests: https://browserbench.org

Building WPEQt
==============

For the time being, WPEQt only targets Linux platforms using graphics drivers
compatible with wayland-egl. Therefore, the end-user Qt application has to
use the wayland-egl Qt QPA plugin. Under certain circumstances the EGLFS QPA
might also work, YMMV.

Using a SVN/git WebKit snapshot
-------------------------------

If you have a `SVN/git development checkout of upstream WebKit`_, then you can build
WPEQt with the following commands on a Linux desktop platform:

::

   $ Tools/wpe/install-dependencies
   $ Tools/Scripts/webkit-flatpak --wpe --wpe-extension=qt update
   $ Tools/Scripts/build-webkit --wpe --cmakeargs="-DENABLE_WPE_QT=ON"

The first command will install the main WPE host build dependencies. The second
command will setup the remaining build dependencies (including Qt5) using
Flatpak. The third command will build WPEWebKit along with WPEQt.

.. _SVN/git development checkout of upstream WebKit: https://trac.webkit.org/wiki/UsingGitWithWebKit#Checkout

Using the WPEWebKit 2.24 source tarball
---------------------------------------

This procedure is already documented in the `WPE Wiki`_ page. The only
change required is the new CMake option for WPEQt, which needs to be
explicitly enabled as follows:

::

   $ cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo -DENABLE_WPE_QT=ON -GNinja

Then, invoke `ninja`, as documented in the Wiki.

.. _WPE Wiki: https://trac.webkit.org/wiki/WPE#BuildingWPEWebKitfromareleasetarball

Using Yocto
-----------

At Igalia_ we're maintaining a `Yocto overlay for WPE`_ (and WebKitGTK). It was
tested for the rocko, sumo and thud Yocto releases. The target platform we
tested so far is the Zodiac RDU2 board, which is based on the Freescale i.MX6
QuadPlus SoC. The backend we used is WPEBackend-fdo which fits very naturally in
the Mesa open-source graphics environment, inside Weston 5. The underlying
graphics driver is etnaviv. In addition to this platform, WPEQt should also run
on Raspberry Pi (with the WPEBackend-rdk or -fdo). Please let us know how it
goes!

.. _Yocto overlay for WPE: https://github.com/igalia/meta-webkit/

To enable WPEQt in meta-webkit, the `qtwpe` option needs to be enabled in the
wpewebkit recipe:

::

  PACKAGECONFIG_append_pn-wpewebkit = " qtwpe"

The resulting OS image can also include WPEQt's sample browser application:

::

  IMAGE_INSTALL_append = " wpewebkit-qtwpe-qml-plugin qt-wpe-simple-browser"

Then, on device, the sample application can be executed either in Weston:

::

  $ qt-wpe-simple-browser -platform wayland-egl https://wpewebkit.org

Or with the EGLFS QPA:

::

  $ # stop weston
  $ qt-wpe-simple-browser -platform eglfs https://wpewebkit.org


Using WPEQt in your application
===============================

A sample MiniBrowser application is included in WebKit, in the
`Tools/MiniBrowser/wpe/qt`_ directory. If you have a desktop build of WPEQt you
can launch it with the following command:

.. _Tools/MiniBrowser/wpe/qt: https://trac.webkit.org/browser/webkit/trunk/Tools/MiniBrowser/wpe/qt

::

   $ Tools/Scripts/run-qt-wpe-minibrowser -platform wayland <url>

Here's the QML code used for the WPEQt MiniBrowser. As you can see it's fairly
straightforward!

::

   import QtQuick 2.11
   import QtQuick.Window 2.11
   import org.wpewebkit.qtwpe 1.0

   Window {
       id: main_window
       visible: true
       width: 1280
       height: 720
       title: qsTr("Hello WPE!")

       WPEView {
           url: initialUrl
           focus: true
           anchors.fill: parent
           onTitleChanged: {
               main_window.title = title;
           }
       }
   }
   

As explained in this blog post, WPEQt is a simple alternative to QtWebKit.
Migrating existing applications should be straightforward because the API
provided by WPEQt is very similar to the QWebView API. We look forward to
hearing your feedback or inquiries on the webkit-wpe_ mailing list and you are
welcome to file bugs in Bugzilla_.

I wouldn't close this post without acknowledging the support of my company
Igalia_ and Zodiac, many thanks to them!

.. _webkit-wpe: https://lists.webkit.org/mailman/listinfo/webkit-wpe
.. _Bugzilla: https://bugs.webkit.org/
.. _QWebView API: https://doc.qt.io/qt-5/qml-qtwebview-webview.html
.. _Igalia: https://igalia.com
