Setting up Debian containers on Fedora Silverblue
#################################################
:tags: Misc
:date: 2020-06-13 13:50:00
:modified: 2020-06-13 13:50:00

After almost 20 years using Debian, I am trying something different, `Fedora
Silverblue`_. However for work I still need to use Debian/Ubuntu from time to
time. In this post I am explaining the steps to setup Debian containers on
Silverblue.

By default Silverblue comes with Toolbox_ which perfectly integrates the OS.
It's a shell script which actually relies on Podman_, an alternative to Docker.
And it works really well with Fedora images! However I ran into various issues
when I wanted to setup Debian containers pulled from docker.io:

::

  $ toolbox create -c sid --image docker.io/debian:sid
  Image required to create toolbox container.
  Download docker.io/debian:sid (500MB)? [y/N]: y
  Created container: sid
  Enter with: toolbox enter --container sid
  $ toolbox enter -c sid
  toolbox: failed to start container sid

This should work, but doesn't, because Toolbox expects specific `Image
requirements`_. After some digging into alternate Toolbox implementations and
quite a bit of experimentation, I found a `Toolbox pull-request adding support
for Debian containers`_. Unfortunately this pull-request wasn't merged yet,
perhaps because the Toolbox developers are busy in the rewrite of Toolbox in Go.
Scrolling down the comments, `Martin Pitt`_ provides some details_ and links to
the approach he's taken to achieve the same goal. Trying to follow his
instructions, I finally managed to have working Debian containers in Silverblue.
Many thanks to him! Here's the run-down

1. Download the build-debian-toolbox script:

::

  $ wget https://piware.de/gitweb/?p=bin.git;a=blob_plain;f=build-debian-toolbox;hb=HEAD
  $ mkdir -p ~/bin
  $ mv build-debian-toolbox ~/bin
  $ chmod +x ~/bin/build-debian-toolbox

2. Modify it a little. Here I had to:

   - change the shell shebang to `/bin/bash`
   - Update toolbox call sites to `~/bin/toolbox`

3. Make sure `~/bin/` is first in your shell `$PATH`
4. Download the patched `toolbox` script from `this pull-request`_ and:

   - Move it to `~/bin/`
   - Again, switch to the `/bin/bash/` shebang in that script

Then run `build-debian-toolbox`. By default it will create a Debian Sid
container, unless you provide override as command-line arguments, so for
instance to create an Ubuntu bionic container you can:

::

  $ build-debian-toolbox bionic ubuntu
  ...
  $ toolbox run -c bionic cat /etc/os-release
  NAME="Ubuntu"
  VERSION="18.04.4 LTS (Bionic Beaver)"
  ID=ubuntu
  ID_LIKE=debian
  PRETTY_NAME="Ubuntu 18.04.4 LTS"
  VERSION_ID="18.04"
  HOME_URL="https://www.ubuntu.com/"
  SUPPORT_URL="https://help.ubuntu.com/"
  BUG_REPORT_URL="https://bugs.launchpad.net/ubuntu/"
  PRIVACY_POLICY_URL="https://www.ubuntu.com/legal/terms-and-policies/privacy-policy"
  VERSION_CODENAME=bionic
  UBUNTU_CODENAME=bionic

That's it! Hopefully the Toolbox Go rewrite will support this out of the box
without requiring third-party tweaks. Thanks again to Martin for his work
on this topic.

.. _Fedora Silverblue: https://silverblue.fedoraproject.org/
.. _Toolbox: https://docs.fedoraproject.org/en-US/fedora-silverblue/toolbox/
.. _Podman: https://podman.io/
.. _Image requirements: https://github.com/containers/toolbox#image-requirements
.. _Toolbox pull-request adding support for Debian containers: https://github.com/containers/toolbox/pull/298
.. _Martin Pitt: https://piware.de/
.. _details: https://github.com/containers/toolbox/pull/298#issuecomment-598591600
.. _this pull-request: https://github.com/containers/toolbox/pull/401
