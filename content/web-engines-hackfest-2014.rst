Web Engines Hackfest 2014
#########################
:tags: WebKit

Last week I attended the `Web Engines Hackfest`_. The
event was sponsored by Igalia (also hosting the event), Adobe and
Collabora.

As usual I spent most of the time working on the WebKitGTK+ GStreamer
backend and `Sebastian Dröge`_ kindly joined and helped out quite a
bit, make sure to read `his post`_ about the event!

We first worked on the WebAudio GStreamer backend, Sebastian cleaned
up various parts of the code, including the playback pipeline and the
source element we use to bridge the WebCore AudioBus with the playback
pipeline. On my side I finished the `AudioSourceProvider patch`_ that was
abandoned for a few months (years) in Bugzilla. It's an interesting
feature to have so that web apps can use the WebAudio API with raw
audio coming from Media elements.

I also hacked on `GstGL support for video rendering`_. It's quite
interesting to be able to share the GL context of WebKit with
GStreamer! The patch is not ready yet for landing but thanks to
the reviews from Sebastian, Mathew Waters and Julien Isorce I'll improve it
and hopefully commit it soon in WebKit ToT.

Sebastian also worked on `Media Source Extensions`_ support. We had a
very basic, non-working, backend that required... a rewrite, basically
:) I hope we will have this `reworked backend`_ soon in
trunk. Sebastian already has it working on Youtube!

The event was interesting in general, with discussions about rendering
engines, rendering and JavaScript.


.. _Web Engines Hackfest: http://www.webengineshackfest.org/
.. _Sebastian Dröge: https://coaxion.net
.. _his post: https://coaxion.net/blog/2014/12/web-engines-hackfest-2014/
.. _AudioSourceProvider patch: https://bugs.webkit.org/show_bug.cgi?id=78883
.. _GstGL support for video rendering: https://bugs.webkit.org/show_bug.cgi?id=138562
.. _Media Source Extensions: http://w3c.github.io/media-source/
.. _reworked backend: https://bugs.webkit.org/show_bug.cgi?id=139441
