Catching up on WebKit GStreamer WebAudio backends maintenance
#############################################################
:tags: Projects, GStreamer, WebKit
:date: 2020-11-29 13:45:00
:modified: 2020-11-29 13:45:00

Over the past few months the WebKit development team has been working on
modernizing support for the WebAudio_ specification. This post highlights some
of the changes that were recently merged, focusing on the GStreamer ports.

My fellow WebKit colleague, Chris Dumez, has been very active lately, updating
the WebAudio implementation for the mac ports in order to comply with the latest
changes of the specification. His contributions have been documented in the
Safari Technology Preview release notes for `version 113`_, `version 114`_,
`version 115`_ and `version 116`_. This is great for the WebKit project! Since
the initial implementation landed around 2011, there wasn't much activity and
over the years our implementation started lagging behind other web engines in
terms of features and spec compliance. So, many thanks Chris, I think you're
making a lot of WebAudio web developers very happy these days :)

The flip side of the coin is that some of these changes broke the GStreamer
backends, as Chris is focusing mostly on the Apple ports, a few bugs slipped in,
noticed by the CI test bots and dutifully gardened_ by our bots sheriffs. `Those
backends were upstreamed in 2012`_ and since then I didn't devote much time to
their maintenance, aside from casual bug-fixing.

One of the WebAudio features recently supported by WebKit is `the Audio Worklet
interface`_ which allows applications to perform audio processing in a dedicated
thread, thus relieving some pressure off the main thread and ensuring a
glitch-free WebAudio rendering. I added support for this feature in r268579_.
Folks eager to test this can try the GTK nightly MiniBrowser with the demos:

::

  $ wget https://trac.webkit.org/export/270226/webkit/trunk/Tools/Scripts/webkit-flatpak-run-nightly
  $ chmod +x webkit-flatpak-run-nightly
  $ python3 webkit-flatpak-run-nightly --gtk MiniBrowser https://googlechromelabs.github.io/web-audio-samples/audio-worklet/

For many years our AudioFileReader implementation was limited to mono and stereo
audio layouts. This limitation was lifted off in r269104_ allowing for
processing of up to 5.1 surround audio files in the AudioBufferSourceNode_.

Our AudioDestination, used for audio playback, was only able to render stereo.
It is now able to probe the GStreamer platform audio sink for the maximum number
of channels it can handle, since r268727_. Support for `AudioContext
getOutputTimestamp`_ was hooked up in the GStreamer backend in r266109_.

The WebAudio spec has a MediaStreamAudioDestinationNode_ for MediaStreams,
allowing to feed audio samples coming from the WebAudio pipeline to outgoing
WebRTC streams. Since r269827_ the GStreamer ports now support this feature as
well! Similarly, incoming WebRTC streams or capture devices can stream their
audio samples to a WebAudio pipeline, this has been supported for a couple years
already, contributed by my colleague Thibault Saunier.

Our GStreamer FFTFrame implementation was broken for a few weeks, while Chris
was landing various improvements for the platform-agnostic and mac-specific
implementations. I finally fixed it in r267471_.

This is only the tip of the iceberg. A few more patches were merged, including
some security-related bug-fixes. As the Web Platform keeps growing, supporting
more and more multimedia-related use-cases, we, at the `Igalia Multimedia team`_,
are committed to maintain our position as GStreamer experts in the WebKit
community.

.. _WebAudio: https://www.w3.org/TR/webaudio/
.. _version 113: https://webkit.org/blog/11294/release-notes-for-safari-technology-preview-113/
.. _version 114: https://webkit.org/blog/11294/release-notes-for-safari-technology-preview-114/
.. _version 115: https://webkit.org/blog/11294/release-notes-for-safari-technology-preview-115/
.. _version 116: https://webkit.org/blog/11294/release-notes-for-safari-technology-preview-116/
.. _gardened: https://trac.webkit.org/wiki/WebKitGTK/Gardening/Howto
.. _Those backends were upstreamed in 2012: https://base-art.net/Articles/1/
.. _the Audio Worklet interface: https://webaudio.github.io/web-audio-api/#audioworklet
.. _r268579: https://trac.webkit.org/changeset/268579
.. _r269104: https://trac.webkit.org/changeset/269104
.. _AudioBufferSourceNode: https://www.w3.org/TR/webaudio/#AudioBufferSourceNode
.. _r268727: https://trac.webkit.org/changeset/268727
.. _AudioContext getOutputTimestamp: https://www.w3.org/TR/webaudio/#dom-audiocontext-getoutputtimestamp
.. _r266109: https://trac.webkit.org/changeset/266109
.. _MediaStreamAudioDestinationNode: https://www.w3.org/TR/webaudio/#mediastreamaudiosourcenode
.. _r269827: https://trac.webkit.org/changeset/269827
.. _r267471: https://trac.webkit.org/changeset/267471
.. _Igalia Multimedia team: https://www.igalia.com/technology/multimedia
