Title: WebRTC in WebKitGTK and WPE, status updates, part I
Date: 2023-02-16 21:30
Tags: Projects, WebKit, GStreamer

<script type="module">
    import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@9/dist/mermaid.esm.min.mjs';
    mermaid.initialize({ startOnLoad: true });
</script>

Some time ago we at [Igalia](https://igalia.com) embarked on the journey to ship
a [GStreamer](https://gstreamer.freedesktop.org)-powered WebRTC backend. This is
a long journey, it is not over, but we made some progress. This post is the
first of a series providing some insights of the challenges we are facing and
the plans for the next release cycle(s).

Most web-engines nowadays bundle a version of [LibWebRTC](https://webrtc.org/),
it is indeed a pragmatic approach. WebRTC is a huge spec, spanning across many
protocols, RFCs and codecs. LibWebRTC is in fact a multimedia framework on its
own, and it's a very big code-base. I still remember the suprised face of
[Emilio](https://crisal.io/) at the [2022 WebEngines
conference](https://webengineshackfest.org/2022/) when I told him we had unusual
plans regarding WebRTC support in GStreamer WebKit ports. There are several
reasons for this plan, explained in the [WPE
FAQ](https://wpewebkit.org/about/faq.html). We worked on a LibWebRTC backend for
the WebKit GStreamer ports, my colleague Thibault Saunier [blogged about
it](https://blogs.gnome.org/tsaunier/2018/07/31/webkitgtk-and-wpe-gains-webrtc-support-back/)
but unfortunately this backend has remained disabled by default and not shipped
in the tarballs, for the reasons explained in the WPE FAQ.

The GStreamer project nowadays provides a library and a plugin allowing
applications to interact with third-party WebRTC actors. This is in my opinion a
paradigm shift, because it enables new ways of interoperability between the
so-called Web and traditional native applications. Since the GstWebRTC
announcement back in late 2017 I've been experimenting with the idea of shipping
an alternative to LibWebRTC in WebKitGTK and WPE. The [initial GstWebRTC WebKit
backend](https://github.com/WebKit/WebKit/commit/a530847a7ce739e1b8b7a55cf1f5ae41a0938a11)
was merged upstream on March 18, 2022.

As you might already know, before any audio/video call, your browser might ask
permission to access your webcam/microphone and even during the call you can now
share your screen. From the WebKitGTK/WPE perspective the procedure is the same
of course. Let's dive in.

## WebCam/Microphone capture

Back in 2018 for the LibWebRTC backend, Thibault added support for
GStreamer-powered media capture to WebKit, meaning that capture devices such as
microphones and webcams would be accessible from WebKit applications using the
[getUserMedia](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getUserMedia)
spec. Under the hood, a GStreamer source element is created, using the
[GstDevice](https://gstreamer.freedesktop.org/documentation/gstreamer/gstdevice.html?gi-language=c)
API. This implementation is now re-used for the GstWebRTC backend, it works
fine, still has room for improvements but that's a topic for a follow-up post.

A MediaStream can be rendered in a `<audio>` or `<video>` element, through a
custom GStreamer source element that we also provide in WebKit, this is all
internally wired up so that the following JS code will trigger the WebView in
natively capturing and rendering a WebCam device using GStreamer:

```html
<html>
  <head>
    <script>
    navigator.mediaDevices.getUserMedia({video: true, audio: false }).then((mediaStream) => {
        const video = document.querySelector('video');
        video.srcObject = mediaStream;
        video.onloadedmetadata = () => {
            video.play();
        };
    });
    </script>
  </head>
  <body>
    <video/>
  </body>
</html>
```

When this WebPage is rendered and after the user has granted access to capture
devices, the GStreamer backends will create not one, but two pipelines.

<pre class="mermaid">
flowchart LR
    pipewiresrc-->videoscale-->videoconvert-->videorate-->valve-->appsink
</pre>

<pre class="mermaid">
flowchart LR
    subgraph mediastreamsrc
    appsrc-->srcghost[src]
    end
    subgraph playbin3
       subgraph decodebin3
       end
       subgraph webkitglsink
       end
       decodebin3-->webkitglsink
    end
    srcghost-->decodebin3
</pre>

The first pipeline routes video frames from the capture device using
`pipewiresrc` to an `appsink`. From the `appsink` our capturer leveraging the
Observer design pattern notifies its observers. In this case there is only one
observer which is a GStreamer source element internal to WebKit called
`mediastreamsrc`. The playback pipeline shown above is heavily simplified, in
reality more elements are involved, but what matters most is that thanks to the
flexibility of Gstreamer, we can leverage the existing MediaPlayer backend that
we at Igalia have been maintaining for more than 10 years, to render
MediaStreams. All we needed was a custom source element, the rest of our
MediaPlayer didn't need much changes to support this use-case.

One notable change we did since the initial implementation though is that for us
a MediaStream can be either raw, encoded or even encapsulated in a RTP payload. So
depending on which component is going to render the MediaStream, we have enough
flexibility to allow zero-copy, in most scenarios. In the example above,
typically the stream will be raw from source to renderer. However, some webcams
can provide encoded streams. WPE and WebKitGTK will be able to internally
leverage these and in some cases allow for direct streaming from hardware device
to outgoing PeerConnection without third-party encoding.

## Desktop capture

There is another JS API, allowing to capture from your screen or a window,
called
[getDisplayMedia](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices/getDisplayMedia),
and yes, we also support it! Thanks to these recent years ground-breaking
progress of the Linux Desktop such as [PipeWire](https://pipewire.org) and
[xdg-desktop-portal](https://flatpak.github.io/xdg-desktop-portal/) we can now
stream your favorite desktop environment over WebRTC. Under the hood when the
WebView is granted access to the desktop capture through the portal, our backend
creates a `pipewiresrc` GStreamer element, configured to source from the file
descriptor provided by the portal, and we have a healthy raw video stream.

Here's a [demo](https://www.youtube.com/live/fAbbiyRnJ6I):

<iframe width="560" height="315" src="https://www.youtube.com/embed/fAbbiyRnJ6I" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## WebAudio capture

What more, yes you can also create a [MediaStream from a WebAudio
node](https://developer.mozilla.org/en-US/docs/Web/API/MediaStreamAudioDestinationNode).
On the backend side, the
[GStreamerMediaStreamAudioSource](https://github.com/WebKit/WebKit/blob/main/Source/WebCore/Modules/webaudio/MediaStreamAudioSourceGStreamer.cpp)
fills GstBuffers from the audio bus channels and notifies third-parties
internally observing the MediaStream, such as outgoing media sources, or simply
an `<audio>` element that was configured to source from the given MediaStream. I
have no demo for this, you'll have to take my word.

## Canvas capture

But wait there is more. Did I hear canvas? Yes we can feed your favorite
`<canvas>` to a MediaStream. The JS API is called
[captureStream](https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/captureStream),
[its code is actually
cross-platform](https://github.com/WebKit/WebKit/blob/main/Source/WebCore/Modules/mediastream/CanvasCaptureMediaStreamTrack.cpp)
but defers to the `HTMLCanvasElement::toVideoFrame()` method which has a
GStreamer implementation. The code is not the most optimal yet though due to
shortcomings of our current graphics pipeline implementation. Here is a [demo of
Canvas to WebRTC](https://www.youtube.com/live/gW0OzeRpzeU) running in the
WebKitGTK MiniBrowser:

<iframe width="560" height="315" src="https://www.youtube.com/embed/gW0OzeRpzeU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>

## Wrap-up

So we've got MediaStream support covered. This is only one part of the puzzle
though. We are facing challenges now on the
[PeerConnection](https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection)
implementation. MediaStreams are cool but it's even better when you can share
them with your friends on the fancy A/V conferencing websites, but we're not
entirely ready for this yet in WebKitGTK and WPE. For this reason, WebRTC is not
yet enabled by default in the upcoming WebKitGTK and WPE 2.40 releases. We're
just not there yet. In the next part of these series I'll tackle the
PeerConnection backend on which we're working hard on these days, both in WebKit
and in GStreamer.

Happy hacking and as always, all my gratitude goes to my fellow
[Igalia](https://igalia.com) comrades for allowing me to keep working on these
domains and to Metrological for funding some of this work. Is your organization
or company interested in leveraging modern WebRTC APIs from WebKitGTK and/or
WPE? If so please [get in touch with us](https://www.igalia.com/contact/) in
order to help us speed-up the implementation work.
