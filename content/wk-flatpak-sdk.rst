Introducing the WebKit Flatpak SDK
##################################
:tags: Projects, WebKit
:date: 2020-06-08 18:50:00
:modified: 2020-06-11 14:50:00

Working on a web-engine often requires a complex build infrastructure. This post
documents our transition from JHBuild to Flatpak_ for the WebKitGTK and
WPEWebKit development builds.

For the last 10 years, WebKitGTK_ has been relying on a custom JHBuild_
moduleset to handle its dependencies and (try to) ensure a reproducible test
environment for the build bots. When WPEWebKit_ was upstreamed several years
ago, a similar approach was used. We ended up with two slightly different
modulesets to maintain. The biggest problem with that is that we still depend on
the host OS for some dependencies. Another set of scripts was then written to
install those, depending on which distro the host is running... This is a bit
unfortunate. There are more issues with JHBuild, when a moduleset is updated,
the bots wipe all the resulting builds and start a new build from scratch! Every
WebKitGTK and WPE developer is strongly advised to use this setup, so everybody
ends up building the dependencies.

In 2018, my colleague `Thibault Saunier`_ worked on a new approach, based on
Flatpak. WebKit could be built as a Flatpak app relying on the GNOME SDK. This
experiment was quite interesting but didn't work for several reasons:

- Developers were not really aware of this new approach
- The GNOME SDK OSTree commits we were pinning we being removed from the
  upstream repo periodically, triggering issues on our side
- Developers still had to build all the WebKit "app" dependencies

In late 2019 I started having a look at this, with a different workflow. I
started to experiment with a custom SDK, based on the `Freedesktop SDK`_. The
goal was to distribute this to WebKit developers, who wouldn't need to worry as
much about build dependencies anymore and just focus on building the damn
web-engine.

I first learned about `flatpak-builder`_, built a SDK with that, and started
playing with it for WebKit builds. I was almost happy with that, but soon
realized flatpak-builder is cool for apps packaging, but for big SDKs, it
doesn't really work out. Flatpak-builder builds a single recipe at a time and if
I want to update one, everything below in the manifest is rebuilt. As the
journey goes on, I found Buildstream_, which is actually used by the FDO and
GNOME folks nowadays. After converting my flatpak-builder manifest to
Buildstream I finally achieved happiness. Buildstream is bit like Yocto,
Buildroot and all the similar NIH sysroot builders. It was one more thing to
learn, but worth it.

The `SDK build definitions`_ are hosted in WebKit's repository. The resulting
Flatpak images are hosted on Igalia's server and locally installed in a custom
Flatpak UserDir so as to not interfere with the rest of the host OS Flatpak
apps. The usual mix of python, perl, ruby WebKit scripts rely on the SDK to
perform their job of building WebKit, running the various test suites (API
tests, layout tests, JS tests, etc). All you need to do is:

1. clone the WebKit git repo
2. run `Tools/Scripts/update-webkit-flatpak`
3. run `Tools/Scripts/build-webkit --gtk`
4. run build artefacts, like the MiniBrowser, `Tools/Scripts/run-minibrowser --gtk`

Under the hood, the SDK will be installed in `WebKitBuild/UserFlatpak` and
transparently used by the various build scripts. The sandbox is started using a
`flatpak run` call which bind-mounts the WebKit checkout and build directory.
This is great! We finally have a unified workflow, not depending on specific
host distros. We can easily update toolchains, package handy debug tools like
rr_, LSP tooling such as ccls_, etc and let the lazy developers actually focus
on development, rather than tooling infrastructure.

Another nice tool we now support, is sccache_. By deploying a "cloud" of
builders in our Igalia_ servers we can now achieve improved build times. The SDK
generates a custom sccache config based on the toolchains it includes (currently
GCC 9.3.0 and clang 8). You can optionally provide an authentication token and
you're set. Access to the Igalia build cloud is restricted to Igalians, but
folks who have their own sccache infra can easily set it up for WebKit. In my
home office where I used to wait more than 20 minutes for a build to complete, I
can now have a build done in around 12 minutes. Once we have Redis-powered cloud
storage we might reach even better results. This is great because the buildbots
should be able to keep the Redis cache warm enough. Also developers who can rely
on this won't need powerful build machines anymore, a standard workstation or
laptop should be sufficient because the heavy C++ compilation jobs happen in the
"cloud".

If you don't like sccache, we still support IceCC_ of course. The main
difference is that IceCC works better on local networks.

Hacking on WebKit often involves hacking on its dependencies, such as GTK,
GStreamer, libsoup and so on. With JHBuild it was fairly easy to vendor patches
in the moduleset. With Buildstream the process is a bit more complicated, but
actually not too bad! As we depend on the FDO SDK we can easily "patch" the
junction file and also adding new dependencies from scratch is quite easy. Many
thanks to the Buildstream developers for the hard work invested in the project!

As a conclusion, all our WebKitGTK and WPEWebKit bots_ are now using the SDK.
JHBuild remains available for now, but on opt-in basis. The goal is to gently
migrate most of our developers to this new setup and eventually JHBuild will be
phased out. We still have some tasks pending, but we achieved very good progress
on improving the WebKit developer workflow. A few more things are now easier to
achieve with this new setup. Stay tuned for more! Thanks for reading!

.. _Flatpak: https://flatpak.org
.. _JHBuild: https://developer.gnome.org/jhbuild/
.. _WebKitGTK: https://webkitgtk.org
.. _WPEWebKit: https://wpewebkit.org
.. _Thibault Saunier: https://blogs.gnome.org/tsaunier/
.. _Freedesktop SDK: https://freedesktop-sdk.io/
.. _flatpak-builder: https://docs.flatpak.org/en/latest/flatpak-builder.html
.. _Buildstream: https://buildstream.build/
.. _SDK build definitions: https://github.com/WebKit/webkit/tree/master/Tools/buildstream
.. _rr: https://rr-project.org
.. _ccls: https://github.com/MaskRay/ccls
.. _sccache: https://github.com/mozilla/sccache
.. _IceCC: https://github.com/icecc/icecream
.. _bots: https://build.webkit.org/
.. _Igalia: https://igalia.com
