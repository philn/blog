#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Philippe Normand'
SITENAME = u'Base-Art'
SITEURL = 'https://base-art.net'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = u'en'
DEFAULT_DATE = 'fs'

# Feed generation is usually not desired when developing
#FEED_ALL_ATOM = None
#CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
CATEGORY_FEED_ATOM = 'feeds/{slug}.atom.xml'
TAG_FEED_ATOM = 'feeds/tag-{slug}.atom.xml'

CATEGORY_FEED_RSS = 'feeds/{slug}.rss.xml'
TAG_FEED_RSS = 'feeds/tag-{slug}.rss.xml'


FEED_MAX_ITEMS = 15

# Blogroll
LINKS = (('Jkx', 'http://larsen-b.com/'),
         ('Igalia', 'https://igalia.com'),
        )

# Social widget
SOCIAL = (('Mastodon', 'https://fosstodon.org/@philn'),
          ('Twitter', 'https://twitter.com/_philn_'),
          ('GitHub', 'https://github.com/philn'),
          ('Last.FM', 'https://last.fm/user/pnormand'),
          ('Feeds', '%s/feeds/' % SITEURL),
         )

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True

ARTICLE_URL = 'Articles/{slug}/'
ARTICLE_SAVE_AS = 'Articles/{slug}/index.html'
TYPOGRIFY = True
THEME = "pelican-themes/pelican-bootstrap3"
#THEME = "new-bootstrap2"
JINJA_ENVIRONMENT = {'extensions': ['jinja2.ext.i18n']}

PLUGIN_PATHS = ['pelican-plugins']
PLUGINS = ['i18n_subsites']

STATIC_PATHS = ['svg', 'png']

#DISQUS_SITENAME = 'base-art'
DISQUS_DISPLAY_COUNTS = True
GITHUB_USER = 'philn'
GITHUB_SHOW_USER_LINK = True
#TWITTER_USERNAME = '_philn_'
#TWITTER_WIDGET_ID='490767776290570240'
FAVICON = 'faviconb.png'
